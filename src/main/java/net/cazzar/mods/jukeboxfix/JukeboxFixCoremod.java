/*
 * Copyright (C) 2013 cazzar
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see [http://www.gnu.org/licenses/].
 */

package net.cazzar.mods.jukeboxfix;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.relauncher.IFMLLoadingPlugin;

import java.util.Map;

/**
 * @Author: Cayde
 */

@Mod(modid = "JukeboxFix", version = "@VERSION@", name = "Jukebox Fix")
@IFMLLoadingPlugin.TransformerExclusions({"net.cazzar.mods.jukeboxfix", "net.cazzar.mods.jukeboxfix.asm"})
public class JukeboxFixCoremod implements IFMLLoadingPlugin {

    /**
     * Return a list of classes that implement the ILibrarySet interface
     *
     * @return a list of classes that implement the ILibrarySet interface
     */
    @Override
    public String[] getLibraryRequestClass() {
        return new String[0];
    }

    /**
     * Return a list of classes that implements the IClassTransformer interface
     *
     * @return a list of classes that implements the IClassTransformer interface
     */
    @Override
    public String[] getASMTransformerClass() {
        return new String[] {"net.cazzar.mods.jukeboxfix.asm.JukeboxTransformer"};
    }

    /**
     * Return a class name that implements "ModContainer" for injection into the mod list The "getName" function should
     * return a name that other mods can, if need be, depend on. Trivially, this modcontainer will be loaded before all
     * regular mod containers, which means it will be forced to be "immutable" - not susceptible to normal sorting
     * behaviour. All other mod behaviours are available however- this container can receive and handle normal loading
     * events
     */
    @Override
    public String getModContainerClass() {
        return null;
    }

    /**
     * Return the class name of an implementor of "IFMLCallHook", that will be run, in the main thread, to perform any
     * additional setup this coremod may require. It will be run <strong>prior</strong> to Minecraft starting, so it CANNOT
     * operate on minecraft itself. The game will deliberately crash if this code is detected to trigger a minecraft class
     * loading (TODO: implement crash ;) )
     */
    @Override
    public String getSetupClass() {
        return null;
    }

    /**
     * Inject coremod data into this coremod This data includes: "mcLocation" : the location of the minecraft directory,
     * "coremodList" : the list of coremods "coremodLocation" : the file this coremod loaded from,
     */
    @Override
    public void injectData(Map<String, Object> data) {
    }
}
