/*
 * Copyright (C) 2013 cazzar
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see [http://www.gnu.org/licenses/].
 */

package net.cazzar.mods.jukeboxfix.asm;

import net.cazzar.corelib.asm.MethodTransformer;
import net.cazzar.corelib.lib.LogHelper;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.*;

/**
 * @Author: Cayde
 */
public class JukeboxTransformer extends MethodTransformer {
    private final String JUKEBOX_CLASS = "net.minecraft.tileentity.TileEntityRecordPlayer";

    public JukeboxTransformer() {
        logger = new LogHelper("JukeboxFix");

        addClass(JUKEBOX_CLASS);

        addMapping("updateEntity", "func_70316_g");
    }

    @Override
    public void transform(ClassNode classNode) {

        InsnList updateEntity = new InsnList();

        updateEntity.add(new MethodInsnNode(Opcodes.INVOKESTATIC, "net/cazzar/corelib/lib/SoundSystemHelper", "getSoundSystem", "()Lpaulscode/sound/SoundSystem;"));
        updateEntity.add(new LdcInsnNode("streaming"));
        updateEntity.add(new LdcInsnNode(new Float(0.5)));
        updateEntity.add(new MethodInsnNode(Opcodes.INVOKESTATIC, "net/cazzar/mods/jukeboxfix/GameSettingHelper", "getSoundVolume", "()F"));
        updateEntity.add(new InsnNode(Opcodes.FMUL));
        updateEntity.add(new MethodInsnNode(Opcodes.INVOKEVIRTUAL, "paulscode/sound/SoundSystem", "setVolume", "(Ljava/lang/String;F)V"));

        updateEntity.add(new InsnNode(Opcodes.RETURN));

        replaceMethod(classNode, "updateEntity", "()V", updateEntity);
    }
}
